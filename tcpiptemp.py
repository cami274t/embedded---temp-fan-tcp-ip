import socket #import socket module
import subprocess
import time

s = socket.socket() #Create a socket object
host = "192.168.137.1" #IP address that the TCP server is
port = 50000 #Reserve a port for your service
s.connect((host, port))

from gpiozero import OutputDevice

ON_THRESHOLD = 50  # (degrees Celsius) Fan kicks on at this temperature.
OFF_THRESHOLD = 45  # (degress Celsius) Fan shuts off at this temperature.
SLEEP_INTERVAL = 5  # (seconds) How often we check the core temperature.
GPIO_PIN = 21  # Which GPIO pin you're using to control the fan.


def get_temp():
    """Get the core temperature.
    Run a shell script to get the core temp and parse the output.
    Raises:
        RuntimeError: if response cannot be parsed.
    Returns:
        float: The core temperature in degrees Celsius.
    """
    output = subprocess.run(['vcgencmd', 'measure_temp'], capture_output=True)
    temp_str = output.stdout.decode()
    try:
        return float(temp_str.split('=')[1].split('\'')[0])
    except (IndexError, ValueError):
        raise RuntimeError('Could not parse temperature output.')


if __name__ == '__main__':
    # Validate the on and off thresholds
    if OFF_THRESHOLD >= ON_THRESHOLD:
        raise RuntimeError('OFF_THRESHOLD must be less than ON_THRESHOLD')

    fan = OutputDevice(GPIO_PIN)

    while True:
        temp = get_temp()

        if temp > ON_THRESHOLD and not fan.value:
            fan.on()

        elif fan.value and temp < OFF_THRESHOLD:
            fan.off()
        time.sleep(SLEEP_INTERVAL)
        print(temp)
        MESSAGE = temp
        s.send(str(MESSAGE).encode())
        
f.close()
print('Succesfully get the file')
s.close()
print('Connection closed')

